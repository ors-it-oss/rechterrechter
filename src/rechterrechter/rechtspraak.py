# -*- coding: UTF-8 -*-
"""


"""
from tornado import gen, httpclient, ioloop, queues
from tornado.httputil import url_concat

try:
    from HTMLParser import HTMLParser
    from urlparse import urljoin, urldefrag
except ImportError:
    from html.parser import HTMLParser
    from urllib.parse import urljoin, urldefrag

BASE_URL = 'http://data.rechtspraak.nl/uitspraken/'

# Logging & Debugging
import logging
import pprint

log = logging.getLogger(__name__)
ppr = pprint.PrettyPrinter(indent=2).pprint


@gen.coroutine
def get_url(url, query):
    """
    
    """
    ppr(query)
    url = url_concat(urljoin(BASE_URL, url), query)
    try:
        response = yield httpclient.AsyncHTTPClient().fetch(url)
        log.debug('fetched %s' % url)

        ppr(response)

        # html = response.body if isinstance(response.body, str) \
        #     else response.body.decode()
        # urls = [urljoin(url, remove_fragment(new_url))
        #         for new_url in get_links(html)]
    except Exception as e:
        print('Exception: %s %s' % (e, url))
        raise gen.Return([])

    raise gen.Return({})
