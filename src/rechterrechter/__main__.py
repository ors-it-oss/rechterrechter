#!/bin/python3
# -*- coding: UTF-8 -*-
'''

rechterrechter

'''

# Python
import os
import sys
import argparse
from argparse import ArgumentError
import pathlib

# 3rd party
import _jsonnet
import json
# import prettytable
# import requests
# import yaml

# Own
import rechterrechter
import rechterrechter.rechtspraak
# import cerberus

# Logging & Debugging
import logging
import pprint

ENV_PREFIX = 'RECHTERRECHTER'
log = logging.getLogger()
logging.basicConfig(
    level=logging.getLevelName(os.environ.get(f'{ENV_PREFIX}_LOGLEVEL', 'info').upper()),
    format='%(levelname)s %(message)s',
    handlers=[logging.StreamHandler(sys.stderr)]
)
ppr = pprint.PrettyPrinter(indent=2).pprint


ARGS = [
    # dict(args=['-H', '--host'], action='store', desc='Gitlab server', required=False, env='GITLAB'),
    dict(args=['-P', '--profile'], action='store', desc='jsonnet profile', required=True, env='PROFILE')
]

COMMANDS = {
    'version': {
        # 'args': [dict(args=['-o', '--online'], action='store_true', desc='Query Gitlab server too')],
        'help': 'Output version'
    },
    'results': {
        'help': 'Look up query in rechtspraak.nl',
        # 'args': [
        #     dict(args=['rosterfile'], type=argparse.FileType('r'), action='store', desc='Roster jsonnet'),
        #     dict(args=['-n', '--dry'], env='DRY', action='store_true', desc='Do not actually do anything')
        # ]
    }
}

# def version():
#     print(f'Cleaning up server {janitor.gl.url}: v{janitor.gl.version()[0]}')


def jnet_load(jnet, vars={}, **kwargs) -> dict:
    '''
    Render stuff through JSONnet

    :param jnet: Snippet or file
    :param vars: Passed on to JSONnet as External Variables
    :param kwargs: Passed on to JSONnet as Top-Level Arguments
    :return: JSONnet generated object
    '''
    for d in kwargs, vars:
        for k, v in d.items():
            d[k] = json.dumps(v)

    if os.path.isfile(pathlib.Path(jnet).resolve()):
        jnobj = _jsonnet.evaluate_file(
            jnet, ext_vars=vars, tla_codes=kwargs
        )
    else:
        jnobj = _jsonnet.evaluate_snippet(
            'snippet', jnet, ext_vars=vars, tla_codes=kwargs
        )
    return json.loads(jnobj)


def cli() -> argparse.ArgumentParser:
    '''
    Command line options.
    '''
    def _add_args(_parser: argparse.ArgumentParser, arg_dicts: list) -> None:
        arg_dict: dict
        for arg_dict in arg_dicts:
            help_txt = arg_dict.pop('desc', False)
            env = arg_dict.pop('env', False)
            if env:
                env = f'{ENV_PREFIX}_{env}'
                help_txt = f'{env}=) {help_txt}'
                default = os.environ.get(env, arg_dict.pop('default', False))
                if default:
                    arg_dict['default'] = default
                    arg_dict.pop('required', None)
            arg_dict['help'] = help_txt
            _parser.add_argument(*arg_dict.pop('args'), **arg_dict)

    parser = argparse.ArgumentParser(
        prog='rechterrechter',
        description='Rechter, rechter! Zeg er eens wat van!',
        formatter_class=argparse.RawDescriptionHelpFormatter)
    _add_args(parser, ARGS)

    parser_commands = parser.add_subparsers(title='Commands', dest='command', required=True)
    for cmd, argdata in COMMANDS.items():
        _args = argdata.pop('args', {})
        parser_cmd = parser_commands.add_parser(cmd, **argdata)
        _add_args(parser_cmd, _args)

    return parser


# '''
# Execution
# '''
if __name__ == "__main__":
    # global args, janitor
    args = cli().parse_args()
    if args.command == 'version':
        print(f'RechterRechter: v{rechterrechter.__version__}')
        sys.exit(0)

    # profile = jnet_load(os.path.join(pathlib.Path(__file__).parent.absolute(), 'schemas.jsonnet'))
    profile = jnet_load(args.profile)

    ppr(args)
    ppr(profile)

    rechterrechter.rechtspraak.get_url('uitspraken', profile['query'])
