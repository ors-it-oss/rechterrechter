#!/bin/bash
(echo "$VERBOSE" | grep -q -e "all" -e "shell") && set -x
set -e -o pipefail

####---------------- HELPERS ----------------####
section_start(){
	section="$1"
	shift
	echo -e "\e[0Ksection_start:`date +%s`:$section\r\e[0K$@"
}

section_end(){
	echo -e "\e[0Ksection_end:`date +%s`:$1\r\e[0K"
}


####---------------- FUNCTIONS ----------------####
rspraak(){
	# -H "Content-type: application/json" \
  # -H "Accept: application/json" \

	# -H "Accept: application/rdf+xml" \
	# -H "Content-type: application/rdf+xml" \

	b_url="https://data.rechtspraak.nl"
	set -x
	curl -Lv \
	$b_url/$1
	set +x
}

clean(){
	rm -rf $CI_PROJECT_DIR/{build,public,src/build} */__pycache__ src/*.egg-info
}

container_build(){
	build_envs="BUILD_FROM_TAG FROM_SCRIPTS_TAG"

	for arg in $build_envs; do
		build_args="$build_args${!arg:+ --build-arg "$arg=${!arg}"}"
	done

	if echo "$VERBOSE" | grep -q -e "all" -e "oci"; then
		build_args="$build_args --log-level debug"
	fi

	buildah bud $build_args \
		-t $CI_COMMIT_REF_NAME -f cicd/Dockerfile .
}


####---------------- EXECUTION ----------------####
if [[ "$CI" != "true" ]]; then
	set -a
	CI_PROJECT_DIR=$PWD

	if [[ -d .git && $(git --version) ]]; then
#		CI_COMMIT_REF_NAME="$(git branch --show-current)"
		CI_COMMIT_REF_NAME="$(git rev-parse --abbrev-ref HEAD)"
	fi
	# BUILD_FROM_TAG=
	# FROM_SCRIPTS_TAG=
#	if which podman >/dev/null; then	# fi
	set +a
fi

if [[ -n "$1" ]]; then
	cmd=$1; shift
	case $cmd in
		*) $cmd "$@" ;;
	esac
fi
